// Copyright (c) 2017, the gRPC project authors. Please see the AUTHORS file
// for details. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:math' show Random;

import 'package:ecommerce_app/src/features/routes-guide/generated/route_guide.pbgrpc.dart';

import 'common.dart';

class RouteGuideService {
  late RouteGuideClient stub;

  RouteGuideService(this.stub);

  String sPrintFeature(Feature feature) {
    final latitude = feature.location.latitude;
    final longitude = feature.location.longitude;
    final name = feature.name.isEmpty ? 'no feature' : 'feature called "${feature.name}"';
    return ('Found $name at ${latitude / coordFactor}, ${longitude / coordFactor}');
  }

  /// Run the getFeature demo. Calls getFeature with a point known to have a
  /// feature and a point known not to have a feature.
  Future<String> runGetFeature() async {
    final sb = StringBuffer();

    final point1 = Point()
      ..latitude = 409146138
      ..longitude = -746188906;
    final point2 = Point()
      ..latitude = 0
      ..longitude = 0;

    sb.writeln(sPrintFeature(await stub.getFeature(point1)));
    sb.writeln(sPrintFeature(await stub.getFeature(point2)));

    return sb.toString();
  }

  /// Run the listFeatures demo. Calls listFeatures with a rectangle containing
  /// all of the features in the pre-generated database. Prints each response as
  /// it comes in.
  Future<String> runListFeatures() async {
    final sb = StringBuffer();

    final lo = Point()
      ..latitude = 400000000
      ..longitude = -750000000;
    final hi = Point()
      ..latitude = 410000000
      ..longitude = -740000000;
    final rect = Rectangle()
      ..lo = lo
      ..hi = hi;

    sb.writeln('Looking for features between 40, -75 and 42, -73');
    await for (var feature in stub.listFeatures(rect)) {
      sb.writeln(sPrintFeature(feature));
    }

    return sb.toString();
  }

  /// Run the recordRoute demo. Sends several randomly chosen points from the
  /// pre-generated feature database with a variable delay in between. Prints
  /// the statistics when they are sent from the server.
  Future<String> runRecordRoute() async {
    final sb = StringBuffer();

    Stream<Point> generateRoute(int count) async* {
      final random = Random();

      for (var i = 0; i < count; i++) {
        final point = featuresDb[random.nextInt(featuresDb.length)].location;
        sb.writeln('Visiting point ${point.latitude / coordFactor}, ${point.longitude / coordFactor}');
        yield point;
        await Future.delayed(Duration(milliseconds: 200 + random.nextInt(100)));
      }
    }

    final summary = await stub.recordRoute(generateRoute(10));
    sb.writeln('Finished trip with ${summary.pointCount} points');
    sb.writeln('Passed ${summary.featureCount} features');
    sb.writeln('Travelled ${summary.distance} meters');
    sb.writeln('It took ${summary.elapsedTime} seconds');

    return sb.toString();
  }

  /// Run the routeChat demo. Send some chat messages, and print any chat
  /// messages that are sent from the server.
  Future<String> runRouteChat() async {
    final sb = StringBuffer();

    RouteNote createNote(String message, int latitude, int longitude) {
      final location = Point()
        ..latitude = latitude
        ..longitude = longitude;
      return RouteNote()
        ..message = message
        ..location = location;
    }

    final notes = <RouteNote>[
      createNote('1st message', 00, 00),
      createNote('2nd message', 00, 10),
      createNote('3rd message', 10, 00),
      createNote('4th message', 10, 10),
    ];

    Stream<RouteNote> outgoingNotes() async* {
      for (final note in notes) {
        // Short delay to simulate some other interaction.
        await Future.delayed(const Duration(milliseconds: 10));
        sb.writeln('Sending message ${note.message} at ${note.location.latitude}, '
            '${note.location.longitude}');
        yield note;
      }
    }

    final call = stub.routeChat(outgoingNotes());
    await for (var note in call) {
      sb.writeln('Got message ${note.message} at ${note.location.latitude}, ${note.location.longitude}');
    }

    return sb.toString();
  }
}