import 'dart:math';

import 'package:ecommerce_app/src/common_widgets/primary_button.dart';
import 'package:ecommerce_app/src/features/products/presentation/home_app_bar/home_app_bar.dart';
import 'package:ecommerce_app/src/features/routes-guide/application/common.dart';
import 'package:ecommerce_app/src/features/routes-guide/application/route-guide-service.dart';
import 'package:ecommerce_app/src/features/routes-guide/generated/route_guide.pbgrpc.dart';
import 'package:ecommerce_app/src/localization/string_hardcoded.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_app/src/common_widgets/responsive_center.dart';
import 'package:ecommerce_app/src/constants/app_sizes.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:grpc/grpc.dart';

/// Shows the product page for a given product ID.
class RouteGuideScreen extends StatelessWidget {
  const RouteGuideScreen({super.key,});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Route Guide'.hardcoded),
      ),
      body: const CustomScrollView(
        slivers: [
          ResponsiveSliverCenter(
            padding: EdgeInsets.all(Sizes.p16),
            child: RouteGuideRunner(),
          ),
        ],
      ),
    );
  }
}

class RouteGuideRunner extends StatefulWidget {
  const RouteGuideRunner({super.key});

  @override
  State<RouteGuideRunner> createState() => _RouteGuideRunnerState();
}

class _RouteGuideRunnerState extends State<RouteGuideRunner> {
  final channel = ClientChannel('127.0.0.1',
      port: 50051, options: const ChannelOptions(credentials: ChannelCredentials.insecure()));

  late RouteGuideClient routeGuideClient;
  late RouteGuideService service;

  String _displayText = '';

  @override
  void initState() {
    super.initState();
    routeGuideClient = RouteGuideClient(channel, options: CallOptions(timeout: const Duration(seconds: 30)));
    print('Client created for channel: ${channel.host}:${channel.port}');
    service = RouteGuideService(routeGuideClient);
  }

  @override
  void dispose() async {
    super.dispose();
    await channel.shutdown();
    print('Channel shutdown.');
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(Sizes.p16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text("GRPC Demo", style: Theme.of(context).textTheme.titleLarge),
            gapH8,
            const Divider(),
            gapH8,
            PrimaryButton(
              onPressed: () async {
                try{
                  var resString = await service.runGetFeature();
                  setState(() {
                    _displayText = resString;
                  });
                } catch(e) {
                  setState(() {
                    _displayText = e.toString();
                  });
                }
              },
              text: 'Simple RPC'.hardcoded,
            ),
            gapH8,
            PrimaryButton(
              onPressed: () async {
                final sb = StringBuffer();

                String sPrintFeature(Feature feature) {
                  final latitude = feature.location.latitude;
                  final longitude = feature.location.longitude;
                  final name = feature.name.isEmpty ? 'no feature' : 'feature called "${feature.name}"';
                  return ('Found $name at ${latitude / coordFactor}, ${longitude / coordFactor}');
                }

                final rect = Rectangle(
                  lo: Point(
                      latitude: 400000000,
                      longitude: -750000000
                  ),
                  hi: Point(
                      latitude:410000000,
                      longitude: -740000000
                  )
                );

                try{
                  sb.writeln('Looking for features between 40, -75 and 42, -73');
                  setState(() {
                    _displayText = sb.toString();
                  });
                  await for (var feature in routeGuideClient.listFeatures(rect)) {
                    sb.writeln(sPrintFeature(feature));
                    setState(() {
                      _displayText = sb.toString();
                    });
                  }
                } catch(e) {
                  setState(() {
                    _displayText = e.toString();
                  });
                }
              },
              text: 'Server-side Streaming RPC'.hardcoded,
            ),
            gapH8,
            PrimaryButton(
              onPressed: () async {
                final sb = StringBuffer();

                Stream<Point> generateRoute(int count) async* {
                  final random = Random();
                  for (var i = 0; i < count; i++) {
                    final point = featuresDb[random.nextInt(featuresDb.length)].location;
                    sb.writeln('Visiting point ${point.latitude / coordFactor}, ${point.longitude / coordFactor}');
                    setState(() {
                      _displayText = sb.toString();
                    });
                    yield point;
                    await Future.delayed(Duration(milliseconds: 200 + random.nextInt(100)));
                  }
                }

                try{
                  final summary = await routeGuideClient.recordRoute(generateRoute(10));
                  sb.writeln('Finished trip with ${summary.pointCount} points');
                  sb.writeln('Passed ${summary.featureCount} features');
                  sb.writeln('Travelled ${summary.distance} meters');
                  sb.writeln('It took ${summary.elapsedTime} seconds');

                  setState(() {
                    _displayText = sb.toString();
                  });
                } catch(e) {
                  setState(() {
                    _displayText = e.toString();
                  });
                }
              },
              text: 'Client-side Streaming RPC'.hardcoded,
            ),
            gapH8,
            PrimaryButton(
              onPressed: () async {
                try{
                  var resString = await service.runRouteChat();
                  setState(() {
                    _displayText = resString;
                  });
                } catch(e) {
                  setState(() {
                    _displayText = e.toString();
                  });
                }
              },
              text: 'Bi-directional Streaming RPC'.hardcoded,
            ),
            gapH8,
            Text(_displayText),
          ],
        ),
      ),
    );
  }
}
